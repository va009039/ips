// VideoRAM.h 2015/5/22
#pragma once

template<class SERIAL_T>
class VideoRAM {
    SERIAL_T& _pc;
    uint16_t x, y;
    bool f_init;

public:
    VideoRAM(RawSerial& pc):_pc(pc),x(0),y(0),f_init(false) {
    }
    void vpoke(uint16_t i, uint8_t b) {
        if (i < 1024) {
            if (x != i%64 || y != i/64) {
                x = i%64;
                y = i/64;
                char buf[16];
                snprintf(buf, sizeof(buf), "\x1b[%d;%dH", y+1, x+1) ; // locate
                _puts(buf);
            }
            _putc(b & 0x7f);
            x++;
        }
    }

private:
    void init() {
        _puts("\x1b[2J"); // erase
    }
    void _puts(const char* s) {
        while(*s) {
            _putc(*s++);
        }
    }
    void _putc(int c) {
        if (!f_init) {
            f_init = true;
            init();
        }
        _pc.putc(c);
    }
};

