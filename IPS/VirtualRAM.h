// VirtualRAM.h 2015/4/29
#pragma once
#define PAGE_CHUNK_SIZE 1024
class VirtualRAM {
public:
    int used_size;
	VirtualRAM(): used_size(0) {
        memset(mem_ptr, 0x00, sizeof(mem_ptr)); // NULL
    }
    uint8_t peek(uint16_t a) {
        uint8_t* mem = ptr(a);
        if (mem == NULL) {
            return 0x00;
        }
        return mem[a % PAGE_CHUNK_SIZE];
    }
    void poke(uint16_t a, uint8_t b) {
        uint8_t* mem = ptr(a);
        if (mem == NULL) {
            mem = reinterpret_cast<uint8_t*>(malloc(PAGE_CHUNK_SIZE));
            MBED_ASSERT(mem != NULL);
            mem_ptr[idx(a)] = mem;
            used_size += PAGE_CHUNK_SIZE;
        }
    	mem[a % PAGE_CHUNK_SIZE] = b;
    }

private:
    uint8_t* ptr(uint16_t addr) {
        return mem_ptr[idx(addr)];
    }
    int idx(uint16_t addr) {
        return addr / PAGE_CHUNK_SIZE;
    }
    uint8_t* mem_ptr[65536 / PAGE_CHUNK_SIZE];
};

