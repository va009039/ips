// BaseIPS.h 2015/5/22
#pragma once

/* data types */
typedef unsigned char u8;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned long u32;
typedef unsigned char byte;

/* some variables of IPS also need to be accessed by the emulator; here are their addresses: */
#define READYFLAG 0x42e
#define LOADFLAG 0x43b
#define UHR 0x418
#define SU0 0x41e
#define SU1 0x422
#define SU2 0x426
#define SU3 0x42a
#define a_PE 0x42f
#define a_PI 0x431
#define a_P1 0x433
#define a_P2 0x435
#define a_P3 0x437
#define a_NDptr 0x43e
#define a_Os 0x43c

#define TVE 1023

class BaseIPS {
public:
    BaseIPS();
    void emulator();
    void load_image(const u8* image, int size);
    void command(const char* s);

    void push_ps(u16 w);
    u16 pull_ps();
    virtual uint8_t mem_peek(uint16_t a) = 0;
    virtual void mem_poke(uint16_t a, uint8_t b) = 0;

    uint8_t peekB(u16 a) { return mem_peek(a); }
    void pokeB(uint16_t a, uint8_t b) { mem_poke(a, b); }

protected:
    void poke(u16 a, u16 w);
    u16 peek(u16 a);
    virtual void do_io() = 0;
    virtual bool test_20ms() { return false; }
    virtual void* file_open(const char* filename, const char* mode = "rb") { return NULL; }
    virtual int file_getc(void* handle) { return -1; }
    virtual void file_putc(int c, void* handle) {}
    virtual bool file_close(void* handle) { return false; }
    virtual void trace(u32 cycle, u16 ppc, u16 hp, u16 cpc, u16 ps, u16 rs) {}
    virtual void usercode(uint16_t cpc) = 0;
    int input_ptr;

private:
    u32 peek32(u16 a);
    u16 PPC, HP, PS, RS;
    u32 cycle;
    int depth;
    int redraw;
    int idle;
    void push_rs(u16 w);
    u16 pull_rs();
    void* inputfile;
    void do_20ms();
    void read_inputfile(void);

    void c_rumpelstilzchen(void); // 0
    void c_defex(void); // 1
    void c_consex(void); // 2
    void c_varex(void); // 3
    void c_retex(void); // 4
    void c_get(void); // 5
    void c_getB(void); // 6
    void c_put(void); // 7
    void c_putB(void); // 8
    void c_1bliteral(void); // 9
    void c_2bliteral(void); // 10
    void c_bronz(void); // 11
    void c_jump(void); // 12
    void c_weg(void); // 13
    void c_pweg(void); // 14
    void c_plus(void); // 15
    void c_minus(void); // 16
    void c_dup(void); // 17
    void c_pdup(void); // 18
    void c_vert(void); // 19
    void c_zwo(void); // 20
    void c_rdu(void); // 21
    void c_rdo(void); // 22
    void c_index(void); // 23
    void c_s_to_r(void); // 24
    void c_r_to_s(void); // 25
    void c_eqz(void); // 26
    void c_gz(void); // 27
    void c_lz(void); // 28
    void c_geu(void); // 29
    void c_f_vergl(void); // 30
    void c_nicht(void); // 31
    void c_und(void); // 32
    void c_oder(void); // 33
    void c_exo(void); // 34
    void c_bit(void); // 35
    void c_cbit(void); // 36
    void c_sbit(void); // 37
    void c_tbit(void); // 38
    void loop_sharedcode(int i);
    void c_jeex(void); // 39
    void c_loopex(void); // 40
    void c_plusloopex(void); // 41
    void c_fieldtrans(void); // 42
    void c_pmul(void); // 43
    void c_pdiv(void); // 44
    void c_tue(void); // 45
    void c_polyname(void); // 46
    void c_scode(void); // 47
    void c_cscan(void); // 48
    void c_chs(void); // 49
    void c_cyc2(void); // 50
    void c_close(void); // 51
    void c_open(void); // 52
    void c_oscli(void); // 53
    void c_load(void); // 54
    void c_save(void); // 55
    void c_setkbptr(void); // 56
    void c_getPS(void); // 57
    void c_setPS(void); // 58
    void c_rp_code(void); // 59
    void c_tr_code(void); // 60
    void c_swap3(void); // 61
    void c_defchar(void); // 62
    void c_pplus(void); // 63
    void c_pminus(void); // 64
};



