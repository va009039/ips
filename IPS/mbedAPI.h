// mbedAPI.h 2015/5/26
#include "BaseIPS.h"
#include "myvector.h"

struct mbedObj {
    void* p;
    uint8_t ct;
};

class mbedAPI {
    BaseIPS& ips;

public:
    mbedAPI(BaseIPS& ips_): ips(ips_) {}
    void code();

private:
    template<class T>
    void init(int ct) {
        push_ps_obj(new T(), ct);
    }

    template<class T, typename A1>
    void init(int ct) {
        A1 a1 = pull_ps<A1>();
        push_ps_obj(new T(a1), ct);
    }

    template<class T, typename A1, typename A2>
    void init(int ct) {
        A2 a2 = pull_ps<A2>();
        A1 a1 = pull_ps<A1>();
        push_ps_obj(new T(a1, a2), ct);
    }

    template<class T, typename A1, typename A2, typename A3>
    void init(int ct) {
        A3 a3 = pull_ps<A3>();
        A2 a2 = pull_ps<A2>();
        A1 a1 = pull_ps<A1>();
        push_ps_obj(new T(a1, a2, a3), ct);
    }

    template<class T, void(T::*member)()>
    void method(void* obj) {
        (reinterpret_cast<T*>(obj)->*member)();
    }

    template<class T, typename A1, void(T::*member)(A1)>
    void method(void* obj) {
        A1 a1 = ips.pull_ps();
        (reinterpret_cast<T*>(obj)->*member)(a1);
    }

    template<class T, typename A1, typename A2, void(T::*member)(A1, A2)>
    void method(void* obj) {
        A2 a2 = ips.pull_ps();
        A1 a1 = ips.pull_ps();
        (reinterpret_cast<T*>(obj)->*member)(a1, a2);
    }

    template<typename R, class T, R(T::*member)()>
    void method(void* obj) {
        ips.push_ps((reinterpret_cast<T*>(obj)->*member)());
    }

    template<typename R, class T, typename A1, R(T::*member)(A1)>
    void method(void* obj) {
        A1 a1 = ips.pull_ps();
        ips.push_ps((reinterpret_cast<T*>(obj)->*member)(a1));
    }

    template<typename T>
    PinName pull_ps();

    void code_method(int f);
    void code_method_I2C(int f, mbedObj& obj);
    mbedObj pull_ps_obj();
    void push_ps_obj(void*, int ct);
    void pull_ps_string(char* buf, size_t size);
    PinName findPinName(const char* name) const;
    myvector<mbedObj> objs;
};


