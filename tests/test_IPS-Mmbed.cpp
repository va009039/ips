// test_IPS-Mmbed.cpp 2015/5/22
#ifdef TEST_IPS_MMBED

#include <new>
#include "mbed.h"
#include "BaseIPS.h"
#include "VideoRAM.h"
#include "VirtualRAM.h"
#include "IPS-Mmbed_bin.h"
#include "mbedAPI.h"

const char* source1 =
": wait_ms 2 $MBEDAPI ;    ""\n"
": DigitalOut 3 $MBEDAPI ; ""\n"
": DigitalIn 4 $MBEDAPI ;  ""\n"
": RawSerial 7 $MBEDAPI ;  ""\n"
": read 8 $MBEDAPI ;       ""\n"
": write 9 $MBEDAPI ;      ""\n"
": putc 10 $MBEDAPI ;      ""\n"

"0 VAR led1                               ""\n"
"\" LED1 \" DigitalOut led1 !             ""\n"
"0 VAR sw1                                ""\n"
"\" USER_BUTTON \" DigitalIn sw1 !        ""\n"
" 0 VAR tty1                              ""\n"
"\" USBTX \" \" USBRX \" RawSerial tty1 ! ""\n"

": main                       ""\n"
"    ANFANG                   ""\n"
"    1 JA?                    ""\n"
"      1 led1 @ write         ""\n"
"      200 wait_ms            ""\n"
"      0 led1 @ write         ""\n"
"      200 wait_ms            ""\n"
"    DANN/NOCHMAL             ""\n"
";                            ""\n"

": main2                      ""\n"
"    ANFANG                   ""\n"
"    1 JA?                    ""\n"
"      sw1 @ read             ""\n"
"      led1 @ write           ""\n"
"    DANN/NOCHMAL             ""\n"
";                            ""\n"

": main3                      ""\n"
"    32 70 JE                 ""\n"
"      I tty1 @ putc          ""\n"
"    NUN                      ""\n"
";                            ""\n"

"main ""\n";

template<class SERIAL_T>
class myips : public BaseIPS {
    VideoRAM<SERIAL_T> vram;
    VirtualRAM mem;
    SERIAL_T& _pc;
    mbedAPI mbedapi;
    Timer t20ms;
    struct FCB {
        const char* buf;
        int size;
        int pos;
    };

public:
    myips(SERIAL_T& pc): vram(pc),_pc(pc),mbedapi(*this) {
        t20ms.reset();
        t20ms.start();
    }
    virtual uint8_t mem_peek(uint16_t a) {
        return mem.peek(a);
    }
    virtual void mem_poke(uint16_t a, uint8_t b) {
        mem.poke(a, b);
        vram.vpoke(a, b);
    }

    virtual void* file_open(const char* filename, const char* mode) {
        FCB* fcb = new FCB;
        fcb->buf = source1;
        fcb->size =  strlen(fcb->buf);
        fcb->pos = 0;
        return (void*)fcb;
    }
    virtual int file_getc(void* handle) {
        MBED_ASSERT(handle);
        FCB* fcb = (FCB*)handle;
        int c = (-1);
        if (fcb->pos < fcb->size) {
            c = fcb->buf[fcb->pos++];
        }
        return c;
    }
    virtual void file_putc(int c, void* handle) {
        MBED_ASSERT(handle);
        MBED_ASSERT(0);
    }
    virtual bool file_close(void* handle) {
        MBED_ASSERT(handle);
        FCB* fcb = (FCB*)handle;
        delete fcb;
        return true;
    }

    virtual bool test_20ms() {
        if (t20ms.read_ms() >= 20) {
            t20ms.reset();
            return true;
        }
        return false;
    }

    virtual void do_io() {
        if (_pc.readable()) {
            int c = _pc.getc();
            if (c == '\n' || c == '\r') {
                if (input_ptr != peek(a_PI)) {
                   pokeB(READYFLAG, 1);
                   poke(a_PE, input_ptr - 1);
                }
            else if (c == 0x08) // BS
                if (input_ptr > 0) {
                    input_ptr--;
                }
            } else {
                if (input_ptr <= TVE) {
                    pokeB(input_ptr++, c);
                }
            }
        }
    }

    virtual void trace(u32 cycle, u16 ppc, u16 hp, u16 cpc, u16 ps, u16 rs) { }
    virtual void usercode(uint16_t cpc) {
        switch(cpc) {
            case 80: break; // c_sleepifidle(void)
            case 96: mbedapi.code(); break;
            default:
                error("code(#%x) not implemented!", cpc);
                /* NOTREACHED */
        }
    }
};

void no_memory () {
  perror("Failed to allocate memory!");
}

int main() {
    std::set_new_handler(no_memory);

    RawSerial pc(USBTX,USBRX);
    pc.baud(115200);
    pc.printf("%s\n", __FILE__);

    myips<RawSerial> ips(pc);
    uint16_t a = 0;
    for(auto b : IPS_Mmbed_bin) {
        ips.pokeB(a++, b);
    }
    ips.command("\" source1.ips \" READ");
    ips.emulator();
    std::exit(1);
}

#endif // TEST_IPS_MMBED
