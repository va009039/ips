// test_bigclock.cpp 2015/5/22
#ifdef TEST_BIGCLOCK

#include <new>
#include "mbed.h"
#include "BaseIPS.h"
#include "VideoRAM.h"
#include "VirtualRAM.h"
#include "IPS-Mu_bin.h"
#include "bigclock_ips.h"

RawSerial pc(USBTX,USBRX);

template<class SERIAL_T>
class myips : public BaseIPS {
    VideoRAM<SERIAL_T> vram;
    VirtualRAM mem;
    SERIAL_T& _pc;
    Timer t20ms;
    size_t rom_ptr;
public:
    myips(RawSerial& pc): vram(pc),_pc(pc) {
        t20ms.reset();
        t20ms.start();
    }
    virtual uint8_t mem_peek(uint16_t a) {
        return mem.peek(a);
    }
    virtual void mem_poke(uint16_t a, uint8_t b) {
        mem.poke(a, b);
        vram.vpoke(a, b);
    }
    virtual void* file_open(const char* filename, const char* mode) {
        rom_ptr = 0;
        return (void*)1;
    }
    virtual int file_getc(void* handle) {
        return rom_ptr < sizeof(bigclock_ips) ? bigclock_ips[rom_ptr++] : (-1);
    }
    virtual bool file_close(void* handle) { return true; }

    virtual void usercode(uint16_t cpc) { }

    virtual bool test_20ms() {
        if (t20ms.read_ms() >= 20) {
            t20ms.reset();
            return true;
        }
        return false;
    }

    virtual void do_io() {
        if (_pc.readable()) {
            int c = _pc.getc();
            if (c == '\n' || c == '\r') {
                if (input_ptr != peek(a_PI)) {
                   pokeB(READYFLAG, 1);
                   poke(a_PE, input_ptr - 1);
                }
            else if (c == 0x08) // BS
                if (input_ptr > 0) {
                    input_ptr--;
                }
            } else {
                if (input_ptr <= TVE) {
                    pokeB(input_ptr++, c);
                }
            }
        }
    }
};

void no_memory () {
  perror("Failed to allocate memory!");
}

int main() {
    std::set_new_handler(no_memory);

    pc.baud(115200);
    pc.printf("%s\n", __FILE__);

    myips<RawSerial> ips(pc);
    ips.load_image(IPS_Mu_bin, sizeof(IPS_Mu_bin));
    ips.command("\" bigclock.ips \" READ");
    ips.emulator();
    /* NOTREACHED */
}

#endif // TEST_BIGCLOCK


