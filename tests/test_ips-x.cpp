// test_ips-x.cpp 2015/5/22
#ifdef TEST_IPS_X

#include <iostream>
#include <new>
#include <string>
#include "mbed.h"
#include "BaseIPS.h"
#include "VideoRAM.h"
#include "VirtualRAM.h"
#include "IPS-Xu_bin.h"
#include "IPS-Mu_bin.h"
#include "IPS-Mu_SRC.h"

#define IPS_TRACE 0
bool f_trace = true;

RawSerial pc(USBTX,USBRX);

template<class SERIAL_T>
class myips : public BaseIPS {
    VideoRAM<SERIAL_T> vram;
    VirtualRAM mem;
    SERIAL_T& _pc;
    Timer t20ms;
    struct FCB {
        string filename;
        string mode;
        size_t pos;
    };
public:
    myips(RawSerial& pc): vram(pc),_pc(pc) {
        t20ms.reset();
        t20ms.start();
    }
    virtual uint8_t mem_peek(uint16_t a) {
        return mem.peek(a);
    }
    virtual void mem_poke(uint16_t a, uint8_t b) {
        mem.poke(a, b);
        vram.vpoke(a, b);
    }
    virtual void* file_open(const char* filename, const char* mode) {
        FCB* fcb = new FCB;
        fcb->filename = filename;
        fcb->mode = mode;
        fcb->pos = 0;
        if (fcb->filename == "IPS-Mu.SRC") {
            MBED_ASSERT(fcb->filename == "IPS-Mu.SRC" && fcb->mode[0] == 'r');
        } else if (fcb->filename == "IPS-Mu.bin") {
            MBED_ASSERT(fcb->filename == "IPS-Mu.bin" && fcb->mode[0] == 'w');
        }
        return (void*)fcb;
    }
    virtual int file_getc(void* handle) {
        MBED_ASSERT(handle);
        FCB* fcb = (FCB*)handle;
        MBED_ASSERT(fcb->filename == "IPS-Mu.SRC");
        int c = (-1);
        if (fcb->pos < sizeof(IPS_Mu_SRC)) {
            c = IPS_Mu_SRC[fcb->pos++];
            pc.putc(c);
        }
        return c;
    }
    virtual void file_putc(int c, void* handle) {
        MBED_ASSERT(handle);
        FCB* fcb = (FCB*)handle;
        MBED_ASSERT(fcb->filename == "IPS-Mu.bin");
        MBED_ASSERT(IPS_Mu_bin[fcb->pos] == c);
        //pc.printf("%s %04x: %02x\n", fcb->filename.c_str(), fcb->ptr, c);
        fcb->pos++;
    }
    virtual bool file_close(void* handle) {
        MBED_ASSERT(handle);
        FCB* fcb = (FCB*)handle;
        MBED_ASSERT(fcb->filename == "IPS-Mu.bin" || fcb->filename == "IPS-Mu.SRC");
        if (fcb->filename == "IPS-Mu.bin") {
            MBED_ASSERT(fcb->pos == sizeof(IPS_Mu_bin));
            f_trace = false;
        }
        delete fcb;
        return true;
    }

    virtual bool test_20ms() {
        if (t20ms.read_ms() >= 20) {
            t20ms.reset();
            return true;
        }
        return false;
    }

    virtual void do_io() {
        if (_pc.readable()) {
            int c = _pc.getc();
            if (c == '\n' || c == '\r') {
                if (input_ptr != peek(a_PI)) {
                   pokeB(READYFLAG, 1);
                   poke(a_PE, input_ptr - 1);
                }
            else if (c == 0x08) // BS
                if (input_ptr > 0) {
                    input_ptr--;
                }
            } else {
                if (input_ptr <= TVE) {
                    pokeB(input_ptr++, c);
                }
            }
        }
    }
#if IPS_TRACE
    virtual void trace(u32 cycle, u16 ppc, u16 hp, u16 cpc, u16 ps, u16 rs) {
        if (f_trace) {
            pc.printf("cycle: %d, ppc: %04x, hp: %04x, cpc: %04x\n",
                cycle, ppc, hp, cpc);
        }
    }
#endif

    virtual void usercode(uint16_t cpc) {
        switch(cpc) {
            case 80: break; // c_sleepifidle(void)
            //case 96: mbedapi.code(); break;
            default:
                error("code(#%x) not implemented!", cpc);
                /* NOTREACHED */
        }
    }
};

void no_memory () {
  std::cout << "Failed to allocate memory!\n";
  std::exit (1);
}

int main() {
    pc.baud(115200);

    std::set_new_handler(no_memory);
    std::cout << __FILE__ << std::endl;

    myips<RawSerial> ips(pc);
    ips.load_image(IPS_Xu_bin, sizeof(IPS_Xu_bin));
    ips.command("~ IPS-Mu.SRC ~ read");
    ips.emulator();
    /* NOTREACHED */
}

#endif // TEST_IPS_X


