// test_ips.cpp 2015/5/14
#ifdef TEST_IPS

#include "mbed.h"
#include "BaseIPS.h"
#include "VideoRAM.h"
#include "VirtualRAM.h"
#include "IPS-Mu_bin.h"
#include "bigclock_ips.h"

RawSerial pc(USBTX,USBRX);

class myips : public BaseIPS {
    VideoRAM vram;
    VirtualRAM mem;
    RawSerial& _pc;
    Timer t20ms;
public:
    myips(RawSerial& pc): vram(pc),_pc(pc) {
        t20ms.reset();
        t20ms.start();
    }
    virtual u8 peekB(u16 a) {
        return mem.peek(a);
    }
    virtual void pokeB(u16 a, u8 b) { 
        mem.poke(a, b);
        vram.vpoke(a, b);
    }

    virtual bool test_20ms() {
        if (t20ms.read_ms() >= 20) {
            t20ms.reset();
            return true;
        }
        return false;
    }

    virtual void do_io() {
        if (_pc.readable()) {
            int c = _pc.getc();
            if (c == '\n' || c == '\r') {
                if (input_ptr != peek(a_PI)) {
                   pokeB(READYFLAG, 1);
                   poke(a_PE, input_ptr - 1);
                }
            else if (c == 0x08) // BS
                if (input_ptr > 0) {
                    input_ptr--;
                    //vram_pos(input_ptr);
                }
            } else {
                if (input_ptr <= TVE) {
                    pokeB(input_ptr++, c);
                }
            }
        }
    }
};

int main() {
    pc.baud(115200);
    pc.printf("%s\n", __FILE__);

    myips* ips = new myips(pc);
    ips->load_image(IPS_Mu_bin, sizeof(IPS_Mu_bin));
    ips->emulator();
    /* NOTREACHED */
}

#endif // TEST_IPS


