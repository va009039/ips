#ifdef TEST_SD

#include <iostream>
#include <new>
#include "mbed.h"
#include "SDFileSystem.h"
#include "mytest.h"

DigitalOut led1(LED1);
SDFileSystem* disk = NULL;
FILE* fp = NULL;

TEST(SD,test1) {
#if defined(TARGET_LPC1768)
    disk = new SDFileSystem(MBED_SPI0, "local");  //  mosi, miso, sclk, cs, name
#else
    disk = new SDFileSystem(D11, D12, D13, D4, "local");  //  mosi, miso, sclk, cs, name  (HW modification candidate)
#endif
    ASSERT_TRUE(disk);
}

const char* path = "/local/readme.txt";

TEST(SD,test2) {
    fp = fopen(path, "a");
    ASSERT_TRUE(fp);
    fprintf(fp, "Hello,world!\n");
    fclose(fp);
}

TEST(SD,test3) {
    fp = fopen(path, "r");
    ASSERT_TRUE(fp);
    char buf[80];
    while(fgets(buf, sizeof(buf), fp) != NULL) {
        //puts(buf);
    }
    fclose(fp);
}

void no_memory () {
  std::cout << "Failed to allocate memory!\n";
  std::exit (1);
}

int main() {
    std::set_new_handler(no_memory);
    std::cout << __FILE__ << std::endl;

    RUN_ALL_TESTS();
    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}

#endif // TEST_SD


