// test_mbedAPI.cpp 2015/5/25
#ifdef TEST_MBEDAPI

#include <new>
#include <map>
#include "mbed.h"
#include "BaseIPS.h"
#include "VideoRAM.h"
#include "VirtualRAM.h"
#include "IPS-Mmbed_bin.h"
#include "mbedAPI.h"

const char* mbed_blinky_ips = // mbed_blinky.ips
": wait_ms 2 $MBEDAPI ;     \n"
": DigitalOut 3 $MBEDAPI ;  \n"
": write 9 $MBEDAPI ;       \n"
"0 VAR led1                 \n"
"\" D9 \" DigitalOut led1 ! \n"
": main                     \n"
"    ANFANG                 \n"
"    1 JA?                  \n"
"      1 led1 @ write       \n"
"      200 wait_ms          \n"
"      0 led1 @ write       \n"
"      200 wait_ms          \n"
"    DANN/NOCHMAL           \n"
";                          \n"
"main                       \n"
"";

const char* app_shield_joystick_ips = // app_shield_joystick.ips
": wait_ms 2 $MBEDAPI ;          \n"
": DigitalOut 3 $MBEDAPI ;       \n"
": DigitalIn 4 $MBEDAPI ;        \n"
": read 8 $MBEDAPI ;             \n"
": write 9 $MBEDAPI ;            \n"

"0 VAR red_led                   \n"
"\" D5 \" DigitalOut red_led !   \n"
"0 VAR blue_led                  \n"
"\" D8 \" DigitalOut blue_led !  \n"
"0 VAR green_led                 \n"
"\" D9 \" DigitalOut green_led ! \n"

"0 VAR up                        \n"
"\" A2 \" DigitalIn up !         \n"
"0 VAR down                      \n"
"\" A3 \" DigitalIn down !       \n"

": main                       \n"
"    ANFANG                   \n"
"    1 JA?                    \n"
"      down @ read            \n"
"      1 EXO                  \n"
"      blue_led @ write       \n"
"    DANN/NOCHMAL             \n"
";                            \n"
"                             \n"
"";

const char* app_shield_pots_ips = // app_shield_pots.ips
": wait_ms 2 $MBEDAPI ;          \n"
": DigitalOut 3 $MBEDAPI ;       \n"
": DigitalIn 4 $MBEDAPI ;        \n"
": read 8 $MBEDAPI ;             \n"
": write 9 $MBEDAPI ;            \n"
": AnalogIn 26 $MBEDAPI ;        \n"
": read_u16 27 $MBEDAPI ;        \n"

"0 VAR pot1                      \n"
"\" A0 \" AnalogIn pot1 !        \n"
"0 VAR pot2                      \n"
"\" A1 \" AnalogIn pot2 !        \n"

": pots                       \n"
"      pot1 @ read_u16        \n"
"      pot2 @ read_u16        \n"
";                            \n"
"                             \n"
"";

const char* app_shield_LM75B_ips = // app_shield_LM75B.ips
": read 8 $MBEDAPI ;             \n"
": write 9 $MBEDAPI ;            \n"
": I2C 15 $MBEDAPI ;             \n"

"0 VAR _i2c                      \n"
"\" D14 \" \" D15 \" I2C _i2c !  \n"
": LM75B #90 _i2c @ ;            \n"
"#0001 VAR buf                   \n"
"buf 2 LM75B write               \n"
": temp                          \n"
"    #00 buf !B                  \n"
"    buf 1 LM75B write           \n"
"    buf 2 LM75B read            \n"
"    buf @B                      \n"
";                               \n"
"";

const char* app_shield_LCD_ips = // app_shield_LCD.ips
": wait_ms 2 $MBEDAPI ;          \n"
": wait_us 28 $MBEDAPI ;         \n"
": DigitalOut 3 $MBEDAPI ;       \n"
": read 8 $MBEDAPI ;             \n"
": write 9 $MBEDAPI ;            \n"
": SPI 14 $MBEDAPI ;             \n"
": format 29 $MBEDAPI ;          \n"

"0 VAR _spi                      \n"
"\" D11 \" \" D12 \" \" D13 \" SPI _spi ! \n"
"0 VAR _reset                    \n"
"\" D8 \" DigitalOut _reset !    \n"
"1 _reset @ write                \n"
"0 VAR _a0                       \n"
"\" D7 \" DigitalOut _a0 !       \n"
"0 VAR _cs                       \n"
"\" D10 \" DigitalOut _cs !      \n"
"1 _cs @ write                   \n"
"0 VAR _cs_sd                    \n"
"\" D4 \" DigitalOut _cs_sd !    \n"
" 1 _cs_sd @ write               \n"

": wr                            \n"
"    _a0 @ write                 \n"
"    0 _cs @ write               \n"
"    _spi @ write WEG            \n"
"    1 _cs @ write               \n"
";                               \n"
": wr_cmd 0 wr ;                 \n"
": wr_dat 1 wr ;                 \n"

" 8 3 _spi @ format ( 8 bit spi mode 3 ) \n"
"1 _reset @ write                \n"
"1 _cs @ write                   \n"
"0 _a0 @ write                   \n"
"0 _reset @ write                \n"
"50 wait_us                      \n"
"1 _reset @ write                \n"

" 5 wait_ms                      \n"

 "#AE wr_cmd ( display off )    \n"
 "#A2 wr_cmd ( bias voltage )   \n"
 "#A0 wr_cmd                    \n"
 "#C8 wr_cmd ( colum normal )   \n"
 "#22 wr_cmd ( voltage resistor ratio ) \n"
 "#2F wr_cmd ( power on )       \n"
 "#40 wr_cmd ( start line = 0 ) \n"
 "#AF wr_cmd ( display ON )     \n"
 "#81 wr_cmd ( set contrast )   \n"
 "#17 wr_cmd ( set contrast )   \n"
 "#A6 wr_cmd ( display normal ) \n"

"( misaki_bdf_b11a.tar.gz misaki_4x8_jisx0201.bdf ) \n"
"384 FELD font                                   \n"
"#0000 #0000 #2F00 #0000 #0003 #0003 #123F #003F \n"
"#3F16 #001A #0812 #0024 #3D32 #002A #0102 #0000 \n"
"#3E00 #0041 #3E41 #0000 #070A #000A #3E08 #0008 \n"
"#4000 #0020 #0808 #0008 #2000 #0000 #0810 #0004 \n"
"#2A1C #001C #3E24 #0020 #2A32 #0024 #2A22 #0014 \n"
"#1418 #003E #2A2E #0012 #2A1C #0012 #3A02 #0006 \n"
"#2A14 #0014 #2A24 #001C #1400 #0000 #1420 #0000 \n"
"#1408 #0022 #1414 #0014 #1422 #0008 #2902 #0006 \n"
"#2912 #001E #093E #003E #253F #001A #211E #0021 \n"
"#213F #001E #253F #0021 #053F #0001 #211E #0039 \n"
"#083F #003F #3F21 #0021 #2010 #001F #043F #003B \n"
"#203F #0020 #063F #003F #013F #003E #211E #001E \n"
"#093F #0006 #211E #005E #093F #0036 #2522 #0019 \n"
"#3F01 #0001 #203F #003F #103F #000F #183F #003F \n"
"#0C33 #0033 #3C03 #0003 #2D31 #0023 #7F00 #0041 \n"
"#3E15 #0015 #7F41 #0000 #0102 #0002 #4040 #0040 \n"
"#0100 #0002 #2418 #003C #243F #0018 #2418 #0024 \n"
"#2418 #003F #2C18 #002C #3F04 #0005 #5448 #003C \n"
"#043F #0038 #3D00 #0000 #3D40 #0000 #083F #0034 \n"
"#3F01 #0000 #1C3C #0038 #043C #0038 #2418 #0018 \n"
"#247C #0018 #2418 #007C #083C #0004 #3C28 #0014 \n"
"#3E04 #0024 #203C #003C #103C #000C #303C #003C \n"
"#1824 #0024 #504C #003C #3424 #002C #3608 #0041 \n"
"#7F00 #0000 #3641 #0008 #0101 #0001 #7F7F #007F \n"
"font 192 !FK                                    \n"

": lcd_page                     \n"
"    #00 wr_cmd                 \n"
"    #10 wr_cmd                 \n"
"    #03 UND #B0 ODER wr_cmd    \n"
";                              \n"

": lcd_locate                       \n"
"    32 /MOD                        \n"
"    4 * DUP                        \n"
"    #0F UND wr_cmd                 \n"
"    16 / #0F UND #10 ODER wr_cmd   \n"
"    #03 UND #B0 ODER wr_cmd        \n"
";                                  \n"

": lcd_cls                      \n"
"    #B0 #B3 JE                 \n"
"      #00 wr_cmd #10 wr_cmd    \n"
"      I wr_cmd                 \n"
"      0 127 JE                 \n"
"        #00 wr_dat             \n"
"      NUN                      \n"
"    NUN                        \n"
";                              \n"

": lcd_putc                 \n"
"    #7F UND                \n"
"    #20 - 4 * font +       \n"
"    DUP 3 +                \n"
"    JE I @B wr_dat NUN     \n"
";                          \n"

": lcd_puts                 \n"
"    ZWO + 1 -              \n"
"    JE I @B lcd_putc NUN   \n"
";                          \n"

"lcd_cls \n"
"3 lcd_locate \n"
"\" mbed application board! \" lcd_puts \n"
"\n";

const char* source1_ips =
": wait_ms 2 $MBEDAPI ;    ""\n"
": DigitalOut 3 $MBEDAPI ; ""\n"
": DigitalIn 4 $MBEDAPI ;  ""\n"
": RawSerial 7 $MBEDAPI ;  ""\n"
": read 8 $MBEDAPI ;       ""\n"
": write 9 $MBEDAPI ;      ""\n"
": putc 10 $MBEDAPI ;      ""\n"

"0 VAR led1                               ""\n"
"\" D9 \" DigitalOut led1 !             ""\n"
"0 VAR sw1                                ""\n"
"\" USER_BUTTON \" DigitalIn sw1 !        ""\n"
" 0 VAR tty1                              ""\n"
"\" USBTX \" \" USBRX \" RawSerial tty1 ! ""\n"

": main                       ""\n"
"    ANFANG                   ""\n"
"    1 JA?                    ""\n"
"      1 led1 @ write         ""\n"
"      200 wait_ms            ""\n"
"      0 led1 @ write         ""\n"
"      200 wait_ms            ""\n"
"    DANN/NOCHMAL             ""\n"
";                            ""\n"

": main2                      ""\n"
"    ANFANG                   ""\n"
"    1 JA?                    ""\n"
"      sw1 @ read             ""\n"
"      led1 @ write           ""\n"
"    DANN/NOCHMAL             ""\n"
";                            ""\n"

": main3                      ""\n"
"    32 70 JE                 ""\n"
"      I tty1 @ putc          ""\n"
"    NUN                      ""\n"
";                            ""\n"

"main ""\n"
"";

std::map<string,const char*> source{
    {string("mbed_blinky.ips"), mbed_blinky_ips},
    {string("app_shield_joystick.ips"), app_shield_joystick_ips},
    {string("app_shield_pots.ips"), app_shield_pots_ips},
    {string("app_shield_LM75B.ips"), app_shield_LM75B_ips},
    {string("app_shield_LCD.ips"), app_shield_LCD_ips},
    {string("source1.ips"), source1_ips},
};

template<class SERIAL_T>
class myips : public BaseIPS {
    VideoRAM<SERIAL_T> vram;
    VirtualRAM mem;
    SERIAL_T& _pc;
    mbedAPI mbedapi;
    Timer t20ms;
    struct FCB {
        const char* buf;
        int size;
        int pos;
    };

public:
    myips(SERIAL_T& pc): vram(pc),_pc(pc),mbedapi(*this) {
        t20ms.reset();
        t20ms.start();
    }
    virtual uint8_t mem_peek(uint16_t a) {
        return mem.peek(a);
    }
    virtual void mem_poke(uint16_t a, uint8_t b) {
        mem.poke(a, b);
        vram.vpoke(a, b);
    }

    virtual void* file_open(const char* filename, const char* mode) {
        MBED_ASSERT(source.count(string(filename)) == 1);
        FCB* fcb = new FCB;
        fcb->buf = source[string(filename)];
        fcb->size =  strlen(fcb->buf);
        fcb->pos = 0;
        return (void*)fcb;
    }
    virtual int file_getc(void* handle) {
        MBED_ASSERT(handle);
        FCB* fcb = (FCB*)handle;
        int c = (-1);
        if (fcb->pos < fcb->size) {
            c = fcb->buf[fcb->pos++];
        }
        return c;
    }
    virtual void file_putc(int c, void* handle) {
        MBED_ASSERT(handle);
        MBED_ASSERT(0);
    }
    virtual bool file_close(void* handle) {
        MBED_ASSERT(handle);
        FCB* fcb = (FCB*)handle;
        delete fcb;
        return true;
    }

    virtual bool test_20ms() {
        if (t20ms.read_ms() >= 20) {
            t20ms.reset();
            return true;
        }
        return false;
    }

    virtual void do_io() {
        if (_pc.readable()) {
            int c = _pc.getc();
            if (c == '\n' || c == '\r') {
                if (input_ptr != peek(a_PI)) {
                   pokeB(READYFLAG, 1);
                   poke(a_PE, input_ptr - 1);
                }
            else if (c == 0x08) // BS
                if (input_ptr > 0) {
                    input_ptr--;
                }
            } else {
                if (input_ptr <= TVE) {
                    pokeB(input_ptr++, c);
                }
            }
        }
    }

    virtual void trace(u32 cycle, u16 ppc, u16 hp, u16 cpc, u16 ps, u16 rs) { }
    virtual void usercode(uint16_t cpc) {
        switch(cpc) {
            case 80: break; // c_sleepifidle(void)
            case 96: mbedapi.code(); break;
            default:
                error("code(#%x) not implemented!", cpc);
                /* NOTREACHED */
        }
    }
};

void no_memory () {
  perror("Failed to allocate memory!");
}

int main() {
    std::set_new_handler(no_memory);

    RawSerial pc(USBTX,USBRX);
    pc.baud(115200);
    //pc.baud(2400);
    pc.printf("%s\n", __FILE__);

    myips<RawSerial> ips(pc);
    uint16_t a = 0;
    for(auto b : IPS_Mmbed_bin) {
        ips.pokeB(a++, b);
    }
    //ips.command("\" mbed_blinky.ips \" READ");
    //ips.command("\" app_shield_joystick.ips \" READ");
    //ips.command("\" app_shield_pots.ips \" READ");
    //ips.command("\" app_shield_LM75B.ips \" READ");
    ips.command("\" app_shield_LCD.ips \" READ");

    ips.emulator();
    std::exit(1);
}

#endif // TEST_MBEDAPI
