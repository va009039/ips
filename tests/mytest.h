// mytest.h 2014/11/6
#pragma once
#include "mbed_debug.h"
//#define debug printf

class BaseTest {
public:
    virtual void _run() = 0;
    const char* m_a;
    const char* m_b;
    int m_line;
    const char* m_file;
    BaseTest* next;
};

class reg {
    BaseTest* head;
    BaseTest* tail;
    reg() {
        head = NULL;
        tail = NULL;
    }
public:
    static reg* inst() {
        static reg regtest;
        return &regtest;
    }
    void add(BaseTest* test) {
        test->next = NULL;
        if (head == NULL) {
            head = test;
            tail = test;
        } else {
            tail->next = test;
            tail = test;
        }
    }
    int run_all_tests(const char* a = "") {
        BaseTest* test = head;
        int pass = 0;
        int count = 0;
        const char* file = "";
        while(test) {
            if (strcmp(a, "") == 0 || strcmp(a, test->m_a) == 0) {
                if (strcmp(file, test->m_file) != 0) {
                    file = test->m_file;
                    debug("%s\n", file);
                }
                debug("TEST(%s,%s)@%d ... ",test->m_a, test->m_b, test->m_line);
                Timer t; t.start();
                test->_run();
                debug("OK (%d ms)\n", t.read_ms());
                pass++;
            }    
            test = test->next;
            count++;
        }
        debug("%d/%d TESTS PASSED!!!\n", pass, count);
        return 0;
    }
};

#define TEST(A,B) \
class class_##A##_##B : public BaseTest { \
public: \
    class_##A##_##B(const char* a, const char* b, const char* file, int line) { \
        m_a = a; m_b = b; \
        m_file = file; m_line = line; \
        reg::inst()->add(this); \
    } \
    virtual void _run(); \
}; \
class_##A##_##B instance_##A##_##B(#A,#B,__FILE__,__LINE__); \
void class_##A##_##B::_run()

#define RUN_TEST(A,B) instance_##A##_##B._run()
#define RUN_ALL_TESTS(A) reg::inst()->run_all_tests(#A)
#define ASSERT_TRUE(A) if(A){}else{debug("\n\n%s@%d %s ASSERT!\n\n",__PRETTY_FUNCTION__,__LINE__,#A);exit(1);};

#define DBG(FMT, ...) do{debug("[%s:%d]"FMT"\r\n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);}while(0);
#define TEST_PRINT(FMT, ...) do{debug("[TEST: %d]"FMT"\r\n", __LINE__, ##__VA_ARGS__);}while(0);
