// test_sd_bigclock.cpp 2015/5/6
#ifdef TEST_SD_BIGCLOCK

#include "mbed.h"
#include "BaseIPS.h"
#include "VideoRAM.h"
#include "VirtualRAM.h"
#include "IPS-Mu_bin.h"
#include "SDFileSystem.h"
#include <string>

#if defined(TARGET_LPC1768)
SDFileSystem local(MBED_SPI0, "local");  //  mosi, miso, sclk, cs, name
#else
SDFileSystem local(D11, D12, D13, D4, "local");  //  mosi, miso, sclk, cs, name  (HW modification candidate)
#endif

RawSerial pc(USBTX,USBRX);

class myips : public BaseIPS {
    VideoRAM vram;
    VirtualRAM mem;
    RawSerial& _pc;
    Timer t20ms;
public:
    myips(RawSerial& pc): vram(pc),_pc(pc) {
        t20ms.reset();
        t20ms.start();
    }
    virtual u8 peekB(u16 a) {
        return mem.peek(a);
    }
    virtual void pokeB(u16 a, u8 b) { 
        mem.poke(a, b);
        vram.vpoke(a, b);
    }
    virtual void* file_open(const char* filename, const char* mode) {
        string path = string("/local/") + filename;
        return fopen(path.c_str(), mode);
    }
    virtual int file_getc(void* handle) {
        int c = fgetc((FILE*)handle);
        return c == EOF ? (-1) : c;
    }
    virtual bool file_close(void* handle) {
        if (fclose((FILE*)handle) == 0) {
            return true;
        }
        return false;
    }

    virtual bool test_20ms() {
        if (t20ms.read_ms() >= 20) {
            t20ms.reset();
            return true;
        }
        return false;
    }

    virtual void do_io() {
        if (_pc.readable()) {
            int c = _pc.getc();
            if (c == '\n' || c == '\r') {
                if (input_ptr != peek(a_PI)) {
                   pokeB(READYFLAG, 1);
                   poke(a_PE, input_ptr - 1);
                }
            else if (c == 0x08) // BS
                if (input_ptr > 0) {
                    input_ptr--;
                }
            } else {
                if (input_ptr <= TVE) {
                    pokeB(input_ptr++, c);
                }
            }
        }
    }
};

int main() {
    pc.printf("%s\n", __FILE__);

    myips* ips = new myips(pc);
    void* fh = ips->file_open("IPS-Mu.bin", "rb");
    MBED_ASSERT(fh);
    if (fh) {
        for(int i = 0;; i++) {
            int c = ips->file_getc(fh);
            if (c == (-1)) {
                break;
            }
            ips->pokeB(i, c);
        }
        ips->file_close(fh);
    }
    //ips->load_image(IPS_Mu_bin, sizeof(IPS_Mu_bin));
    ips->command("\" bigclock.ips \" READ");
    ips->emulator();
    /* NOTREACHED */
}

#endif // TEST_SD_BIGCLOCK


