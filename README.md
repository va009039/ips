# IPS(Interpreter for Process Structures) for mbed #

IPSをmbedで動くようにしてみました。linux/unix版を元にしています。


```
( mbed_blinky.ips )
0 VAR led1
" LED1 " DigitalOut led1 !
: main
    ANFANG
    1 JA?
      1 led1 @ write
      200 wait_ms
      0 led1 @ write
      200 wait_ms
    DANN/NOCHMAL
;
main
```