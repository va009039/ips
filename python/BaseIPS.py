# BaseIPS.py 2015/5/23
# coding: utf-8
import abc
import logging
import sys

READYFLAG = 0x42e
LOADFLAG  = 0x43b
UHR       = 0x418
a_PE      = 0x42f
a_PI      = 0x431
a_P1      = 0x433
a_P2      = 0x435
a_P3      = 0x437
a_NDptr   = 0x43e
a_Os      = 0x43c

JUMP_RET  = 0
JUMP_EXEC = 1

code_name = [
   "c_rumpelstilzchen", # 0
   "c_defex",           # 1
   "c_consex",          # 2
   "c_varex",           # 3
   "c_retex",           # 4
   "c_get",             # 5
   "c_getB",            # 6
   "c_put",             # 7
   "c_putB",            # 8
   "c_1bliteral",       # 9
   "c_2bliteral",       # 10
   "c_bronz",           # 11
   "c_jump",            # 12
   "c_weg",             # 13
   "c_pweg",            # 14
   "c_plus",            # 15
   "c_minus",           # 16
   "c_dup",             # 17
   "c_pdup",            # 18
   "c_vert",            # 19
   "c_zwo",             # 20
   "c_rdu",             # 21
   "c_rdo",             # 22
   "c_index",           # 23
   "c_s_to_r",          # 24
   "c_r_to_s",          # 25
   "c_eqz",             # 26
   "c_gz",              # 27
   "c_lz",              # 28
   "c_geu",             # 29
   "c_f_vergl",         # 30
   "c_nicht",           # 31
   "c_und",             # 32
   "c_oder",            # 33
   "c_exo",             # 34
   "c_bit",             # 35
   "c_cbit",            # 36
   "c_sbit",            # 37
   "c_tbit",            # 38
   "c_jeex",            # 39
   "c_loopex",          # 40
   "c_plusloopex",      # 41
   "c_fieldtrans",      # 42
   "c_pmul",            # 43
   "c_pdiv",            # 44
   "c_tue",             # 45
   "c_polyname",        # 46
   "c_scode",           # 47
   "c_cscan",           # 48 0x30
   "c_chs",             # 49
   "c_cyc2",            # 50
   "c_close",           # 51
   "c_open",            # 52
   "c_oscli",           # 53
   "c_load",            # 54
   "c_save",            # 55
   "c_setkbptr",        # 56
   "c_getPS",           # 57
   "c_setPS",           # 58
   "c_rp_code",         # 59
   "c_tr_code",         # 60
   "c_swap3",           # 61
   "c_defchar",         # 62
   "c_pplus",           # 63
   "c_pminus",          # 64
   "c_rumpelstilzchen", # 65
   "c_rumpelstilzchen", # 66
   "c_rumpelstilzchen", # 67
   "c_rumpelstilzchen", # 68
   "c_rumpelstilzchen", # 69
   "c_rumpelstilzchen", # 70
   "c_rumpelstilzchen", # 71
   "c_rumpelstilzchen", # 72
   "c_rumpelstilzchen", # 73
   "c_rumpelstilzchen", # 74
   "c_rumpelstilzchen", # 75
   "c_rumpelstilzchen", # 76
   "c_rumpelstilzchen", # 77
   "c_rumpelstilzchen", # 78
   "c_rumpelstilzchen", # 79
   "c_sleepifidle"      # 80
   ]

def CAST_S16(u16):
    if 0x7fff >= u16 >= 0:
        return u16
    elif 0xffff >= u16 >= 0x8000:
        return (0x10000 - u16) * -1
    return u16

class BaseIPS(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.ppc = 0
        self.hp = 0
        self.ps = 0xfff8
        self.rs = 0x04f8
        self.table = []
        self.cycle = 0
        self.input_ptr = 512
        self.inputfile = None
        self.exit_flag = False

    @abc.abstractmethod
    def mem_poke(self, a, b):
        pass

    @abc.abstractmethod
    def mem_peek(self, a):
        pass

    @abc.abstractmethod
    def file_open(self, filename, mode):
        pass

    @abc.abstractmethod
    def file_close(self, handle):
        return False

    @abc.abstractmethod
    def file_getc(self, handle):
        return -1

    @abc.abstractmethod
    def file_putc(self, c, handle):
        pass

    @abc.abstractmethod
    def trace(self, cycle, ppc, hp, cpc, ps, rs):
        pass

    @abc.abstractmethod
    def usercode(self, cpc):
        pass

    def do_io(self):
        pass

    def do_20ms(self):
        pass

    def pokeB(self, a, b):
        self.mem_poke(a, b)

    def peekB(self, a):
        return self.mem_peek(a)

    def poke(self, a, w):
        assert 0xffff >= a >= 0
        if w < 0:
            w += 65536
        assert 0xffff >= w >= 0
        self.pokeB(a, w & 0xff)
        self.pokeB(a+1, w>>8)

    def peek(self, a):
        return self.peekB(a) | self.peekB(a+1)<<8

    def peek32(self, a):
        return self.peekB(a) | self.peekB(a+1)<<8 | self.peekB(a+2)<<16 | self.peekB(a+3)<<24

    def push_ps(self, w):
        self.ps -= 2
        self.poke(self.ps, w)

    def pull_ps(self):
        w = self.peek(self.ps)
        self.ps += 2
        return w

    def push_rs(self, w):
        self.rs -= 2
        self.poke(self.rs, w)

    def pull_rs(self):
        w = self.peek(self.rs)
        self.rs += 2
        return w

    def c_rumpelstilzchen(self): # 0 high level no-op
        return JUMP_RET # return to emulator

    def c_defex(self): # 1 DEFinition EXecutive
        self.push_rs(self.ppc) # push return address to return stack
        self.ppc = self.hp # jump to code at hp
        return JUMP_RET # return to emulator

    def c_consex(self): # 2 CONStant EXecutive
        self.push_ps(self.peek(self.hp)) # value is at hp; push to ps
        return JUMP_RET # return to emulator

    def c_varex(self): # 3 VAREX VARiable EXecutive
        self.push_ps(self.hp) # hp is address of variable; push to ps
        return JUMP_RET # return to emulator

    def c_retex(self): # 4 RETEX REturn EXecutive
        self.ppc = self.pull_rs() # jump back to caller
        return JUMP_RET # return to emulator

    def c_get(self): # 5 @ Get word at addr on stack; place word on stack
        self.poke(self.ps, self.peek(self.peek(self.ps)))
        return JUMP_RET # return to emulator

    def c_getB(self): # 6 @B Get word at addr on stack; place word on stack
        self.poke(self.ps, self.peekB(self.peek(self.ps)))
        return JUMP_RET # return to emulator

    def c_put(self): # 7 ! Put word on stack to address on stack
        a = self.pull_ps()
        v = self.pull_ps()
        self.poke(a, v)
        return JUMP_RET # return to emulator

    def c_putB(self): # 8 !B Put byte on stack to address on stack
        a = self.pull_ps()
        v = self.pull_ps()
        self.pokeB(a, v)
        return JUMP_RET # return to emulator

    def c_1bliteral(self): #9 1BLITERAL Number executive, 1 byte
        self.push_ps(self.peekB(self.ppc)) # get byte at ppc; push to ps
        self.ppc += 1 # skip over literal's value
        return JUMP_RET # return to emulator

    def c_2bliteral(self): # 10 2BLITERAL Number executive, 2 byte
        self.push_ps(self.peek(self.ppc)) # get word at ppc; push to ps
        self.ppc += 2 # skip over literal's value
        return JUMP_RET # return to emulator

    def c_bronz(self): # 11 BRONZ If ps = 0 jump to addr, else carry on
        if (self.pull_ps() & 1) == 0:
            self.ppc = self.peek(self.ppc) # jump to address
        else:
            self.ppc += 2 # jump over address
        return JUMP_RET # return to emulator

    def c_jump(self): # 12 JUMP jump to address
        self.ppc = self.peek(self.ppc)
        return JUMP_RET # return to emulator

    def c_weg(self): # 13
        self.ps += 2;
        return JUMP_RET # return to emulator

    def c_pweg(self): # 14 PWEG Remove 2 items from the stack
        self.ps += 4 # adjust ps pointer
        return JUMP_RET # return to emulator

    def c_plus(self): # 15 + Add two numbers on the stack
        v = self.pull_ps() # <n> <m> ---> <n+m>
        self.poke(self.ps, self.peek(self.ps) + v)
        return JUMP_RET # return to emulator

    def c_minus(self): # 16 - Subtract two numbers on the stack
        v = self.pull_ps() # <n> <m> ---> <n-m>
        self.poke(self.ps, self.peek(self.ps) - v)
        return JUMP_RET # return to emulator

    def c_dup(self): # 17 DUP Duplicate item on stack
        self.push_ps(self.peek(self.ps)) # <n> ---> <n> <n>
        return JUMP_RET # return to emulator
 
    def c_pdup(self): # 18 PDUP Duplicate 2 items on stack
        self.push_ps(self.peek(self.ps+2)) # <n> <m> ---> <n> <m> <n>
        self.push_ps(self.peek(self.ps+2)) # <n> <m> <n> ---> <n> <m> <n> <m>
        return JUMP_RET # return to emulator

    def c_vert(self): # 19
        h = self.peek(self.ps)
        self.poke(self.ps, self.peek(self.ps + 2))
        self.poke(self.ps + 2, h)
        return JUMP_RET # return to emulator

    def c_zwo(self): # 20 ZWO Duplicate second stack item
        self.push_ps(self.peek(self.ps + 2)) # <n> <m> ---> <n> <m> <n>
        return JUMP_RET # return to emulator

    def c_rdu(self): # 21
        h = self.peek(self.ps)
        self.poke(self.ps, self.peek(self.ps+2))
        self.poke(self.ps+2, self.peek(self.ps+4))
        self.poke(self.ps+4, h)
        return JUMP_RET # return to emulator

    def c_rdo(self): # 22
        h = self.peek(self.ps + 4)
        self.poke(self.ps + 4, self.peek(self.ps + 2))
        self.poke(self.ps + 2, self.peek(self.ps))
        self.poke(self.ps, h)
        return JUMP_RET # return to emulator

    def c_index(self): # 23 I Copy top of return stack to parameter stack
        self.push_ps(self.peek(self.rs))
        return JUMP_RET # return to emulator

    def c_s_to_r(self): # 24
        self.push_rs(self.pull_ps())
        return JUMP_RET # return to emulator

    def c_r_to_s(self): # 25
        self.push_ps(self.pull_rs())
        return JUMP_RET # return to emulator

    def c_eqz(self): # 26
        if self.peek(self.ps) == 0:
            self.poke(self.ps, 1)
        else:
            self.poke(self.ps, 0)
        return JUMP_RET # return to emulator

    def c_gz(self): # 27 >0 Relational tests; values are signed
        v = self.peek(self.ps)
        if CAST_S16(v) > 0: # >0
            self.poke(self.ps, 1)
        else:
            self.poke(self.ps, 0)
        return JUMP_RET # return to emulator

    def c_lz(self): # 28
        v = self.peek(self.ps)
        if CAST_S16(v) < 0: # <0
            self.poke(self.ps, 1)
        else:
            self.poke(self.ps, 0)
        return JUMP_RET # return to emulator

    def c_geu(self): # 29
        b = self.pull_ps()
        a = self.peek(self.ps)
        self.poke(self.ps, 1 if a >= b else 0)
        return JUMP_RET # return to emulator


    def c_nicht(self): # 31
        self.poke(self.ps, 0xffff^self.peek(self.ps))
        return JUMP_RET # return to emulator

    def c_und(self): # 32 Logical AND, OR, EXOR words on stack
        v = self.pull_ps() # pull first value
        self.poke(self.ps, v & self.peek(self.ps))  # where OP = AND/OR/EXOR
        return JUMP_RET # return to emulator

    def c_oder(self): # 33
        v = self.pull_ps()
        self.poke(self.ps, v | self.peek(self.ps))
        return JUMP_RET # return to emulator

    def c_exo(self): # 34
        v = self.pull_ps()
        self.poke(self.ps, v ^ self.peek(self.ps))
        return JUMP_RET # return to emulator


    def loop_sharedcode(self, i):
        l = CAST_S16(self.peek(self.rs))
        if i <= l:
            self.push_rs(i)
            self.ppc = self.peek(self.ppc)
        else:
            self.pull_rs();
            self.ppc += 2

    def c_jeex(self): # 39
        self.ppc = self.peek(self.ppc)
        self.push_rs(self.pull_ps())
        self.loop_sharedcode(CAST_S16(self.pull_ps()))
        return JUMP_RET # return to emulator

    def c_loopex(self): # 40
        self.loop_sharedcode(CAST_S16((self.pull_rs() + 1)))
        return JUMP_RET # return to emulator

    def c_plusloopex(self): # 41
        i = CAST_S16(self.pull_rs())
        self.loop_sharedcode(i + CAST_S16(self.pull_rs()))
        return JUMP_RET # return to emulator

    def c_fieldtrans(self): # 42
        n = self.pull_ps()
        d = self.pull_ps()
        s = self.pull_ps()
        while 1:
            b = self.peekB(s)
            s += 1
            self.pokeB(d, b)
            d += 1
            n = (n-1)&0xff
            if n == 0:
                break
        return JUMP_RET # return to emulator

    def c_pmul(self): # 43
        c = self.pull_ps() * self.pull_ps()
        self.push_ps(c&0xffff)
        self.push_ps(c>>16)
        return JUMP_RET # return to emulator

    def c_pdiv(self): # 44
        d = self.pull_ps()
        nh = self.pull_ps()
        nl = self.pull_ps()
        n = (nh<<16) + nl;
        if d == 0:
            q = 0xffff
            r = 0
        else:
            q = n / d
            r = n % d
            if q >= 0x10000:
                q = 0xffff
                r = 0
        self.push_ps(q)
        self.push_ps(r)
        return JUMP_RET # return to emulator

    def c_tue(self): # 45
        self.hp = self.pull_ps()
        return JUMP_EXEC

    def c_polyname(self): # 46 $POLYNAME name hasher
        d = self.pull_ps()
        x = self.pull_ps()
        p = self.pull_ps()
        x = x | (p&0xff00) | ((p&0xff)<<16)
        p = d ^ x ^ (x>>1) ^ (x>>2) ^ (x>>7)
        x = x ^ ((p&0xff)<<24)
        x >>= 7
        self.push_ps( (x&0xff00) | ((x>>16)&0xff) )
        self.push_ps( (x&0xff) )
        return JUMP_RET # return to emulator

    def c_scode(self): # 47
        offs = self.peek(a_Os)
        i = self.peek(a_NDptr)
        nd = self.peek32(i)
        i = self.peek(a_P3)
        while i:
            i += offs
            if ( (self.peek32(i) ^ nd) & 0xffffff3f) == 0:
                self.push_ps(i+6)
                return JUMP_RET
            i = self.peek(i + 4)
        self.push_ps(0)
        return JUMP_RET # return to emulator

    def c_cscan(self): # 48
        comment = 0
        pi = self.peek(a_PI)
        pe = self.peek(a_PE)
        a = self.pull_ps()
        if  a == 1:
            while pi <= pe:
                if comment:
                    if self.peekB(pi) == ord(')'):
                        comment = 0
                else:
                    if self.peekB(pi) == ord('('):
                        comment = 1
                    elif self.peekB(pi) != ord(' '):
                       self.poke(a_PI, pi)
                       self.push_ps(1)
                       return JUMP_RET
                pi += 1
            self.push_ps(0)
            self.poke(a_P2, 1)
        else:
            while pi <= pe:
                if self.peekB(pi) == ord(' '):
                    break
                pi += 1
            self.poke(a_PI, pi)
            self.push_ps(0)
        return JUMP_RET # return to emulator


    def c_close(self): # 51
        if self.inputfile:
            self.file_close(self.inputfile)
            self.inputfile = None
        return JUMP_RET # return to emulator

    def c_open(self): # 52 
        if self.inputfile:
            self.file_close(self.inputfile)
        namelen = self.pull_ps()
        namestart = self.pull_ps()
        assert namelen <= 255
        filename = ""
        for i in range(namelen):
            filename += chr(self.peekB(namestart + i))
        self.inputfile = self.file_open(filename, "r")
        if not self.inputfile:
            self.push_ps(0)
        else:
            self.push_ps(1)
        return JUMP_RET # return to emulator

    def c_save(self): # 55
        namelen = self.pull_ps()
        namestart = self.pull_ps()
        end = self.pull_ps()
        start = self.pull_ps()
        assert namelen <= 255
        filename = ""
        for i in range(namelen):
            filename += chr(self.peekB(namestart + i))
        fh = self.file_open(filename, "wb")
        if fh == None:
            self.push_ps(0)
            return JUMP_RET # return to emulator
        for i in range(start, end):
            b = self.peekB(i)
            self.file_putc(b, fh)
        self.file_close(fh)
        self.push_ps(1)
        return JUMP_RET # return to emulator

    def c_setkbptr(self): # 56
        self.input_ptr = self.pull_ps() & 0x3ff
        return JUMP_RET # return to emulator

    def c_getPS(self): # 57
        self.push_ps(self.ps)
        return JUMP_RET # return to emulator

    def c_setPS(self): # 58
        self.ps = self.pull_ps()
        return JUMP_RET # return to emulator


    def emulator(self, start_ppc = 0x400):
        self.ppc = start_ppc
        while 1:
            self.hp = self.peek(self.ppc)
            self.ppc += 2
            while 1:
                cpc = self.peek(self.hp)
                self.hp += 2
                self.trace(self.cycle, self.ppc-2, self.hp-2, cpc, self.ps, self.rs)
                if cpc == 0: r = self.c_rumpelstilzchen()
                elif cpc == 1: r = self.c_defex()
                elif cpc == 2: r = self.c_consex()
                elif cpc == 3: r = self.c_varex()
                elif cpc == 4: r = self.c_retex()
                elif cpc == 5: r = self.c_get()
                elif cpc == 6: r = self.c_getB()
                elif cpc == 7: r = self.c_put()
                elif cpc == 8: r = self.c_putB()
                elif cpc == 9: r = self.c_1bliteral()
                elif cpc == 10: r = self.c_2bliteral()
                elif cpc == 11: r = self.c_bronz()
                elif cpc == 12: r = self.c_jump()
                elif cpc == 13: r = self.c_weg()
                elif cpc == 14: r = self.c_pweg()
                elif cpc == 15: r = self.c_plus()
                elif cpc == 16: r = self.c_minus()
                elif cpc == 17: r = self.c_dup()
                elif cpc == 18: r = self.c_pdup()
                elif cpc == 19: r = self.c_vert()
                elif cpc == 20: r = self.c_zwo()
                elif cpc == 21: r = self.c_rdu()
                elif cpc == 22: r = self.c_rdo()
                elif cpc == 23: r = self.c_index()
                elif cpc == 24: r = self.c_s_to_r()
                elif cpc == 25: r = self.c_r_to_s()
                elif cpc == 26: r = self.c_eqz()
                elif cpc == 27: r = self.c_gz()
                elif cpc == 28: r = self.c_lz()
                elif cpc == 29: r = self.c_geu()
                elif cpc == 31: r = self.c_nicht()
                elif cpc == 32: r = self.c_und()
                elif cpc == 33: r = self.c_oder()
                elif cpc == 34: r = self.c_exo()
                elif cpc == 39: r = self.c_jeex()
                elif cpc == 40: r = self.c_loopex()
                elif cpc == 42: r = self.c_fieldtrans()
                elif cpc == 43: r = self.c_pmul()
                elif cpc == 44: r = self.c_pdiv()
                elif cpc == 45: r = self.c_tue()
                elif cpc == 46: r = self.c_polyname()
                elif cpc == 47: r = self.c_scode()
                elif cpc == 48: r = self.c_cscan()
                elif cpc == 51: r = self.c_close()
                elif cpc == 52: r = self.c_open()
                elif cpc == 55: r = self.c_save()
                elif cpc == 56: r = self.c_setkbptr()
                elif cpc == 57: r = self.c_getPS()
                elif cpc == 58: r = self.c_setPS()
                else:
                    self.usercode(cpc)
                    r = JUMP_RET
                self.cycle += 1
                if r == JUMP_RET:
                    break
            self.do_io()
            self.do_20ms()
            if self.peekB(READYFLAG) == 0 and self.peekB(LOADFLAG) == 1:
                self.read_inputfile()

            if self.exit_flag:
                break

    def read_inputfile(self):
        for i in range(0x200):
            self.pokeB(0x200 + i, 0x20)
        while 1:
            ch = self.file_getc(self.inputfile)
            if ch < 1:
                if self.input_ptr == self.peek(a_PI):
                    self.file_close(self.inputfile)
                    self.inputfile = None
                    self.pokeB(LOADFLAG, 0)
                    self.input_ptr = 0x200
                break
            elif ch == ord("\r"):
                pass
            elif ch == ord("\n"):
                self.input_ptr = (self.input_ptr & 0xffc0) + 64
            else:
                self.pokeB(self.input_ptr, ch)
                self.input_ptr += 1
            if self.input_ptr >= 1024:
                break

        if self.input_ptr != self.peek(a_PI):
            self.pokeB(READYFLAG, 1)
            self.poke(a_PE, self.input_ptr - 1)

    def load_image(self, img):
        for i,c in enumerate(img):
            self.pokeB(i, ord(c))

    def command(self, s):
        if len(s) >= 1:
            for i,c in enumerate(s):
                self.pokeB(i + 0x200, ord(c))
            self.poke(a_PI, 0x200)
            self.poke(a_PE, 0x200 + len(s) - 1)
            self.pokeB(READYFLAG, 1)

    def writeable(self):
        return self.peekB(READYFLAG) == 0

    def puts(self, s):
        if len(s) >= 1:
            for i,c in enumerate(s):
                self.pokeB(i + 0x200, ord(c))
            self.poke(a_PI, 0x200)
            self.poke(a_PE, 0x200 + len(s) - 1)
            self.pokeB(READYFLAG, 1)

if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)
    class ips(BaseIPS):
        def __init__(self):
            super(ips, self).__init__()


