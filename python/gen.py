#coding: UTF-8
# gen.py 2015/5/23

def apiips(filename):
    s = "( mbed_api.ips )\n"
    with open(filename, 'r') as f:
        for line in f.readlines():
            p = line.split()
            if len(p) >= 3 and p[0] == "#define" and p[1].startswith("mbed_"):
                s += ": %s %s $MBEDAPI ;\n" % (p[1].replace("mbed_",""), p[2])
    return s

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='+')
    parser.add_argument('--api', action='store_true', default=False)
    args = parser.parse_args()
    for i, filename in enumerate(args.infiles):
        if args.api:
            print apiips(filename)

