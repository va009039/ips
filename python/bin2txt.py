#coding: UTF-8
# bin2txt.py 2015/5/6

def bin2txt(filename, f_cpp=True):
    with open(filename, 'rb') as f:
        data = f.read()
        varname = filename.replace('.', '_').replace('-', '_')
        if f_cpp:
            s = "const uint8_t %s[] = { // %s\n" % (varname, filename)
        else:
            s = "%s = [ # %s\n" %  (varname, filename)
        for i,c in enumerate(data):
            s += "0x%02x," % ord(c)
            if i % 16 == 15:
                s += "\n"
        if f_cpp:
            s += "};\n"
        else:
            s += "]\n"
    return s

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='+')
    parser.add_argument('--cpp', action='store_true', default=False)
    parser.add_argument('--python', action='store_true', default=False)
    args = parser.parse_args()
    for i, filename in enumerate(args.infiles):
        print bin2txt(filename, args.cpp)

