# ips-x.py 2015/5/23
# coding:utf-8
#
import sys
import logging
import BaseIPS
import IPS_Mu_bin

class FCB:
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode
        self.pos = 0

class myips(BaseIPS.BaseIPS):
    def __init__(self):
        super(myips, self).__init__()
        self.mem = [0]*65536

    def mem_poke(self, a, b):
        assert 65535 >= a >= 0
        assert 255 >= b >= 0
        self.mem[a] = b
        if 0x01d5 <= a <= 0x01ef:
            c = chr(b) if 0x7e >= b >= 0x20 else "."
            logging.info("cycle: %d a:%04x b:%02x c:%s" % (self.cycle, a, b, c))

    def mem_peek(self, a):
        assert 65535 >= a >= 0
        b = self.mem[a]
        assert 255 >= b >= 0
        return self.mem[a]

    def file_open(self, filename, mode):
        if filename == "IPS-Mu.bin":
            return FCB(filename, mode)
        return open(filename, mode)

    def file_close(self, handle):
        assert handle != None
        if isinstance(handle, FCB):
            fcb = handle
            assert fcb.filename == "IPS-Mu.bin"
            assert fcb.pos == len(IPS_Mu_bin.image)
            return True
        handle.close()
        return True

    def file_getc(self, handle):
        assert handle != None
        c = handle.read(1)
        if len(c) == 0:
            return -1
        sys.stdout.write(c)
        return ord(c)

    def file_putc(self, c, handle):
        assert handle != None
        if isinstance(handle, FCB):
            fcb = handle
            assert fcb.filename == "IPS-Mu.bin"
            assert c == IPS_Mu_bin.image[fcb.pos]
            fcb.pos += 1
            return
        handle.write(chr(c))
        return

    def trace(self, cycle, ppc, hp, cpc, ps, rs):
        #logging.info("cycle:%d ppc:%04x hp:%04x cpc:%04x ps:%04x rs:%04x" % (cycle, ppc, hp, cpc, ps, rs))
        if cycle > 450000:
            self.exit_flag = True

    def usercode(self, cpc):
        if cpc == 80: # c_sleepifidle()
            pass
        else:
            logging.error("%s(%d) not implemented!" % (BaseIPS.code_name[cpc], cpc))
            assert 0

def ips_x(file):
    ips = myips()
    image = "IPS-Xu.bin"
    logging.info("image=%s" % image)
    with open(image, "rb") as f:
        ips.load_image(f.read())
    cmd = '~ %s ~ read' % file
    logging.info("cmd=[%s]" % cmd)
    ips.command(cmd)
    ips.emulator()
    

if __name__=="__main__":
    logging.basicConfig(level=logging.INFO, filename="R:/ips-x.log")
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='+')
    args = parser.parse_args()
    for i, filename in enumerate(args.infiles):
        ips_x(filename)

