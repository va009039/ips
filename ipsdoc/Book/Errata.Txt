Errors in book  "IPS - High Level Programming of Small Systems" by Karl Meinzer
(1978), ISBN 0-9530507-0-X


Page 6.    Three references to  -50  should be  -40 

Page 39.   Ex. h; reference to NOT should be INV

Page 69.   Diagram.  In H2INC the $H just before 1BLITERAL should not be there.

Page 74.   Compiler action matrix; the numbers #40 and #80  should be swapped.

Page 90.   Add to table:          WEG/AB        DEL/FROM

Page 104.  Remarks.  #DF should be #D3         



Last updated:

2005 Dec 08 [Thu] 1607 utc by JRM

<end>
